import React from 'react';

const Shares  = props => {
  return (
     <div>
      {
        props.loading === true ?
            <div className="preloader-wrapper big active">
                <div className="spinner-layer spinner-blue-only">
                  <div className="circle-clipper left">
                    <div className="circle"></div>
                  </div>
                  <div className="gap-patch">
                    <div className="circle"></div>
                  </div>
                  <div className="circle-clipper right">
                    <div className="circle"></div>
                  </div>
                </div>
            </div> : 
        Object.keys(props.last.data).length !== 0 ?
            <div className="buscador row">
                <table >
                  <thead>
                    <tr> 
                        <th>Date for last update</th>
                        <th>Open</th>
                        <th>High</th>
                        <th>Low</th>
                        <th>Close</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr> 
                      <td>{props.last.last}</td>
                      <td>{props.last.data["1. open"]}</td>
                      <td>{props.last.data["2. high"]}</td>
                      <td>{props.last.data["3. low"]}</td>
                      <td>{props.last.data["4. close"]}</td> 
                      { 
                          props.diferencia.type === "D" ? 
                          <td className="less">{props.diferencia.perc}%</td>  
                          :
                          <td className="more">{props.diferencia.perc}%</td>
                      } 
                      { 
                          props.diferencia.type === "D" ? 
                          <td className="less">USD {props.diferencia.value}</td>  
                          :
                          <td className="more">USD {props.diferencia.value}</td>
                      } 
                      { 
                          props.diferencia.type === "D" ? 
                          <td className="less"><i class="material-icons">arrow_downward</i></td>  
                          :
                          <td className="more"><i class="material-icons">arrow_upward</i></td>
                      } 
                    </tr> 
                  </tbody>
                </table>
            </div>
        : null
      }
     </div>

    )
}

export default Shares;
