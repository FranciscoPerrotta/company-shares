import React from 'react'

const Form = props => { 

  var shareRef = React.createRef();

  const consultShares = e => {

    e.preventDefault(); 
    props.consultShares(shareRef.current.value);

  }

  return (
      <div className="buscador row">
          <div className="col s12 m8 offset-m2">
              <form onSubmit={consultShares}> 
                  <div className="input-field col s12 m8">
                    <select ref={shareRef}>
                      <option value="null" defaultValue>Select a company</option>
                      <option value="FB">Facebook(FB)</option>
                      <option value="AAPL">Apple (AAPL)</option>
                      <option value="MSFT">Microsoft (MSFT)</option>
                      <option value="GOOGL">Google (GOOGL)</option>
                      <option value="AMZN">Amazon (AMZN)</option> 
                    </select>
                  </div>
                  <div className="input-field col s12 m4 enviar">
                    <input type="submit" className="btn  red lighten-2" value="Consult"/>
                  </div>
              </form>
          </div>
      </div>
    )
}

export default Form;
