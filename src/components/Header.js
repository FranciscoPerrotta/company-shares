import React from 'react';

const Header = props => {
    return (
        <nav>
            <div className="nav-wrapper red lighten-2">
                <a href="/" className="brand-logo center">{props.title}</a>
            </div>
        </nav>
    )
}

export default Header;