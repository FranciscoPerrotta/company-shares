import React, { Component } from 'react';
import './css/App.css';

// Components
import Header from './components/Header';
import Footer from './components/Footer';
import Form from './components/Form';
import Shares from './components/Shares';

class App extends Component {

  state = {
    shares: {}, 
    diferencia: { perc: 0, value: 0, type: ""},
    last: {last: '', data: {}},
    yesterday: {last: '', data: {}},
    loading: false
  }

  fetchShares = async (url) => await (await fetch(url)).json();

  changeShares = (shares) => { this.setState({shares}) };
  changeLoading = () => { this.setState({loading:!this.state.loading}) };
  changeLast = (last) => { this.setState({last}) };
  changeYesterday = (yesterday) => { this.setState({yesterday}) }; 
  changeDiferencia = (diferencia) => { this.setState({diferencia}) };

  consultShares = async (company) => {
  
    if(company !== 'null'){

      this.changeLoading(); 

      const url = `https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=${company}&outputsize=full&apikey=X86NOH6II01P7R24`
      
      const response =  await this.fetchShares(url);
  
      this.changeShares(response); 

      // Para obtener las key del json que llega 
      var DaysKeys = Object.keys(this.state.shares["Time Series (Daily)"]);

      // Key de los últimos datos registrados
      var data = this.state.shares["Time Series (Daily)"][DaysKeys[0]]; 
      // Key de los datos del dia anterior a los ultimos datos registrados 
      var data2 = this.state.shares["Time Series (Daily)"][DaysKeys[1]]; 

      this.changeLast({last: DaysKeys[0], data});
      this.changeYesterday({last: DaysKeys[1], data: data2});

      this.changeLoading();
      this.compare();
    }  

  } 

  compare = () => {

    var resultado = 0; 

    const yesterdayPrice = this.state.yesterday.data["4. close"];
    const actualPrice = this.state.last.data["4. close"];
  
    if(yesterdayPrice !== actualPrice){

      var o = 0;

      if(yesterdayPrice > actualPrice){

        o = yesterdayPrice - actualPrice;

        resultado = (o / yesterdayPrice) * 100;
       
        resultado = this.trunc(resultado,2); 
        o = this.trunc(o,2);

        this.changeDiferencia({ perc: resultado, value: o, type: "D"})
      
      } else {

        o = actualPrice - yesterdayPrice;

        resultado = (o / yesterdayPrice) * 100;
       
        resultado = this.trunc(resultado,2);
        o = this.trunc(o,2);

        this.changeDiferencia({ perc: resultado, value: o, type: "A"})

      }
    }  

  }
  
  // Funcion para obtener solo dos decimales
  trunc = (number, position = 0) => {
    var s = number.toString() 
    var decimalLength = s.indexOf('.') + 1
    var numStr = s.substr(0, decimalLength + position)
    return Number(numStr)
  }

  render() {
    return (
      <div className="App">
          <Header
            title="Company shares"
          /> 
          <div className="container white contenedor-acciones">
            <Form 
              consultShares={this.consultShares}
            /> 
            <Shares
              loading={this.state.loading}
              empresa={this.state.empresa}
              last={this.state.last}
              yesterday={this.state.yesterday}
              diferencia={this.state.diferencia}
            />
          </div>
          <Footer/> 
      </div>
    );
  }
}

export default App;
